# Language: pt

Funcionalidade: Compartilhamento de documento

Como usuário do WhatsApp
Eu quero poder compartilhar documentos com os meus contatos
Para distribuir um documento de forma mais simples

Cenário: Compartilhamento de um documento
Dado que eu esteja na tela de mensagens do whatsapp
E selecione a conversa com o contato que eu quero enviar um documento
Quando estiver na tela de conversa
E clicar no icone Clips
E clicar no icone Documento
E clicar em um dos documentos da lista
Então deve ser exibido a mensagem "Enviar NomeDoDocumento para o NomeDoContato?"
E a opção Cancelar
E a opção Enviar
E clicando na opção Enviar o arquivo deve ser enviado para o contato