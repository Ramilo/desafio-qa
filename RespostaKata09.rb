class Rule

  def initialize(itemName, unitPrice, productQuantitySpecialPrice, specialPriceValue)

    @itemName = itemName

    @unitPrice = unitPrice

    @productQuantitySpecialPrice = productQuantitySpecialPrice

    @specialPriceValue = specialPriceValue

  end


  def itemName

    @itemName

  end


  def checkoutSum(productQuantity)

    if(@productQuantitySpecialPrice != 0 && @specialPriceValue != 0)

      withDiscount = (productQuantity / @productQuantitySpecialPrice).floor * @specialPriceValue

      withoutDiscount = (productQuantity % @productQuantitySpecialPrice) * @unitPrice

      return withDiscount + withoutDiscount

    else

      return productQuantity * @unitPrice

    end

  end

end


class Checkout

  def initialize(*rules)

    @rules = rules

    @scanned = []

  end


  def scan(name)

    @scanned.push(name)

  end


  def total()

    groupedValues = Hash.new(0)

    sum = 0

    @scanned.each { | name | groupedValues.store(name, groupedValues[name]+1) }

    groupedValues.each { |name, productQuantity|

      selectedRule = @rules.find { |rule| rule.itemName == name }

      sum += selectedRule.checkoutSum(productQuantity)

    }

    return sum

  end

end