# Language: pt

Funcionalidade: Enviar mensagem de Voz

Como usuário do WhatsApp
Eu quero poder enviar uma mensagem de voz em uma conversa
Para não ter que ficar digitando a mensagem

Cenário: Enviando uma mensagem de Voz
Dado que eu esteja na tela de uma conversa no whatsapp
Quando eu manter precionado o icone do microfone
E falar a mensagem "Isso é um teste de mensagem de voz"
Então ao soltar o botão do microfone a mensagem grava deve ser enviada